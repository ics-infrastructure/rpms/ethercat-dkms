%define module_name ethercat
%define version 1.5.2.ESS1
# Top of stable-1.5 is not tagged
%define repo_version 334c34cfd2e5

Name:           %{module_name}-%{_driver}-dkms
Version:        %{version}
Release:        1
Summary:        DKMS-ready kernel-source for the ehercat driver
License:        GPL
URL:            http://hg.code.sf.net/p/etherlabmaster/code
Patch0:         ethercat_service_in.patch
Patch1:         e1000e-patch-due-to-rh-kabi.p0.patch
BuildRequires:  autoconf libtool gcc gcc-c++ mercurial systemd
Requires:       systemd
Requires(pre):  dkms
Requires(post): dkms
Buildroot:      %{_tmppath}/%{name}-%{version}-root

%description
This is an open-source EtherCAT master implementation for Linux

%prep
hg clone -u %{repo_version} %{url} .
%patch0 -p1
%if %{_driver} == "e1000e"
%patch1 -p0
%endif

%build
%if %{_driver} == "generic"
%define driver_options --enable-generic --disable-e1000e
%define driver_module_path devices/ec_generic
%else
%if %{_driver} == "e1000e"
%define driver_options --disable-generic --enable-e1000e --with-e1000e-kernel=3.10
%define driver_module_path devices/%{_driver}/ec_%{_driver}
%endif
%endif
./bootstrap
# Without --disable-kernel configure will try to find the kernel sources
# for the running kernel. We don't want to build kernel modules now.
# They are built by dkms after installation on the machine.
./configure --prefix=/usr --sysconfdir=/etc --disable-kernel \
  %{driver_options} \
  --disable-8139too \
  --disable-e100 \
  --disable-e1000 \
  --disable-igb \
  --disable-r8169 \
  --disable-ccat \
  --enable-eoe=no \
  --enable-cycles=yes \
  --enable-sii-assign=yes \
  --enable-hrtimer=no
make

%clean
rm -fr $RPM_BUILD_ROOT

%install
# Copy all sources required for dkms to build the modules
mkdir -p %{buildroot}/usr/src/%{module_name}-%{version}-%{release}
cp -a * %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/

# Install binaries and libraries
make DESTDIR=${RPM_BUILD_ROOT} install
# Remove unwanted files to avoid: error: Installed (but unpackaged) file(s) found
# /etc/init.d/ethercat and /etc/sysconfig/ethercat are not used with systemd
# /etc/ethercat.conf is managed separately
rm -f ${RPM_BUILD_ROOT}/etc/ethercat.conf ${RPM_BUILD_ROOT}/etc/init.d/ethercat ${RPM_BUILD_ROOT}/etc/sysconfig/ethercat
# Create /opt/etherlab link
mkdir -p ${RPM_BUILD_ROOT}/opt
ln -s /usr ${RPM_BUILD_ROOT}/opt/etherlab

# Create udev rule to give normal users read/write access
mkdir -p ${RPM_BUILD_ROOT}/etc/udev/rules.d
echo 'KERNEL=="EtherCAT[0-9]*", MODE="0666"' > ${RPM_BUILD_ROOT}/etc/udev/rules.d/99-ethercat.rules

# Create dkms.conf file
cat > %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/dkms.conf <<EOF

DEST_MODULE_LOCATION[0]="/kernel/drivers/net/wireless/"
PACKAGE_NAME="%{module_name}"
PACKAGE_VERSION="%{version}-%{release}"

MAKE[0]="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build modules"
CLEAN="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build clean"

BUILT_MODULE_NAME[0]="master/ec_master"
DEST_MODULE_NAME[0]="ec_master"
DEST_MODULE_LOCATION[0]="/kernel/drivers/%{module_name}"

BUILT_MODULE_NAME[1]="%{driver_module_path}"
DEST_MODULE_NAME[1]="ec_%{_driver}"
DEST_MODULE_LOCATION[1]="/kernel/drivers/%{module_name}"

AUTOINSTALL="yes"
REMAKE_INITRD="no"
EOF

%post
%udev_rules_update
dkms add -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms build -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms install -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
%systemd_post ethercat.service

%preun
%systemd_preun ethercat.service
dkms remove -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade --all ||:

%postun
%udev_rules_update
%systemd_postun_with_restart ethercat.service

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc COPYING.LESSER
%doc ChangeLog
%doc FEATURES
%doc INSTALL
%doc NEWS
%doc README
%doc README.EoE
/usr/src/%{module_name}-%{version}-%{release}
/usr/include/*.h
/usr/lib/libethercat.a
/usr/lib/libethercat.la
/usr/bin/ethercat
/usr/lib/libethercat.so*
/usr/lib/systemd/system/ethercat.service
/usr/sbin/ethercatctl
/etc/udev/rules.d/99-ethercat.rules
/opt/etherlab
