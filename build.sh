#!/bin/bash
set -e

# Download sources
spectool -g -C ./SOURCES SPECS/ethercat-dkms.spec
# Build RPMs
for driver in generic e1000e
do
  rpmbuild -v --clean --define "%_topdir `pwd`" --define "_driver $driver" -bb SPECS/ethercat-dkms.spec
  rm -rf BUILD
done

tree RPMS

# Remove unwanted RPM
rm -f RPMS/x86_64/ethercat-*dkms-debuginfo-*.rpm
