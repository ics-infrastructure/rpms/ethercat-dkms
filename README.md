# ethercat-dkms

CentOS RPM for EtherLab [IgH EtherCAT Master](https://etherlab.org/en/ethercat/)

This RPM includes:

- the kernel modules managed by dkms (ec_master and ec_generic)
- ethercat binary and libraries
- ethercat systemd service
- udev rule to give read/write access to all users

Note that the `/etc/ethercat.conf` file is not part of this RPM. It should be managed separately (by Ansible).


To build a new version of the RPM:

- update the version in the spec file
- push your changes to check that the RPM can be built by gitlab-ci
- tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo
